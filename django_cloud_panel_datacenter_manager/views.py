from django.http import HttpResponse
from django.template import loader
from django.utils.translation import gettext as _

from django_cloud_panel_main.django_cloud_panel_main.views import get_nav
from django_datacenter_manager_client.django_datacenter_manager_client.func import *
from django_session_ldap_attributes.django_session_ldap_attributes.decorators import (
    admin_group_required,
)
from django_session_ldap_attributes.django_session_ldap_attributes.snippets import (
    get_mdat_customer_for_user,
)


@admin_group_required("NETWORK")
def list_virtual_networks(request):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/list_virtual_networks.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = _("List Virtual Networks")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def edit_virtual_network(request, virtual_network):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/edit_virtual_network.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = _("Edit Virtual Network")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def modal_virtual_network_delete(request, virtual_network):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/modal_virtual_network_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = (
        _("Delete Virtual Network") + " " + virtual_network
    )

    template_opts["virtual_network"] = virtual_network

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def modal_virtual_network_create(request):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/modal_virtual_network_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Compute Cloud")
    template_opts["content_title_sub"] = _("Create Virtual Network")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def list_routers(request):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/list_routers.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("List Routers")

    template_opts["menu_groups"] = get_nav(request)

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    try:
        all_routers = datacenter_manager_get_ipsubnets(mdat.id)
    except:
        all_routers = ()

    template_opts["all_routers"] = []

    for router in all_routers:
        new_routers = {
            "network": router["network"],
            "id": router["router"],
            "router": datacenter_manager_get_ip_info(mdat.id, int(router["router"]))[
                "ip_address"
            ],
            "reverse_fqdn": datacenter_manager_get_ip_info(
                mdat.id, int(router["router"])
            )["reverse_fqdn"],
            "date_changed": router["date_changed"],
        }
        template_opts["all_routers"].append(new_routers)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def edit_router(request, router_id):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/edit_router.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("Edit Router")

    template_opts["menu_groups"] = get_nav(request)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def modal_router_delete(request, router):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/modal_router_delete.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("Delete Router") + " " + router

    template_opts["router"] = router

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def modal_router_create(request):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/modal_router_create.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("Create Router")

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def list_ipsubnets(request):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/list_ipsubnets.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("IP Address Management")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    try:
        all_subnets = datacenter_manager_get_ipsubnets(mdat.id)
    except:
        all_subnets = ()

    template_opts["ipsubnets"] = []

    for subnet in all_subnets:
        new_subnet = {
            "id": subnet["id"],
            "network": subnet["network"],
            "router": datacenter_manager_get_ip_info(mdat.id, int(subnet["router"]))[
                "ip_address"
            ],
            "vlan": datacenter_manager_get_vlan(mdat.id, int(subnet["vlan"])),
            "date_changed": subnet["date_changed"],
        }
        template_opts["ipsubnets"].append(new_subnet)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def list_ipsubnet_info(request, subnet_id):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/list_ipsubnet_reservations.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("IP Address Management")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)
    template_opts["reservations"] = datacenter_manager_get_ipsubnet(mdat.id, subnet_id)

    return HttpResponse(template.render(template_opts, request))


@admin_group_required("NETWORK")
def list_vlans(request):
    template = loader.get_template(
        "django_cloud_panel_datacenter_manager/list_vlans.html"
    )

    template_opts = dict()

    template_opts["content_title_main"] = _("Network Management")
    template_opts["content_title_sub"] = _("VLAN Management")

    template_opts["menu_groups"] = get_nav(request)

    mdat = get_mdat_customer_for_user(request.user)

    try:
        template_opts["all_vlans"] = datacenter_manager_get_vlans(mdat.id)
    except:
        template_opts["all_vlans"] = ()

    return HttpResponse(template.render(template_opts, request))
