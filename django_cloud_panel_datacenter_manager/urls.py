from django.urls import path

from . import views

urlpatterns = [
    path("virtual-networks", views.list_virtual_networks),
    path("virtual-networks/create", views.modal_virtual_network_create),
    path("virtual-network/<str:virtual_network>", views.edit_virtual_network),
    path(
        "virtual-network/<str:virtual_network>/delete",
        views.modal_virtual_network_delete,
    ),
    path("routers", views.list_routers),
    path("routers/create", views.modal_router_create),
    path("router/<int:router_id>", views.edit_router),
    path("router/<int:router_id>/delete", views.modal_router_delete),
    path("ipsubnets", views.list_ipsubnets),
    path("ipsubnet/<int:subnet_id>", views.list_ipsubnet_info),
    path("vlans", views.list_vlans),
]
